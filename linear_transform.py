import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Polygon
import matplotlib.colors as mcolors

def plot_vectors(vector_names):
  c=list(mcolors.TABLEAU_COLORS.values())
  fig, ax = plt.subplots()
 

  # Move left y-axis and bottim x-axis to centre, passing through (0,0)
  ax.spines['left'].set_position('center')
  ax.spines['bottom'].set_position('center')

  # Eliminate upper and right axes
  ax.spines['right'].set_color('none')
  ax.spines['top'].set_color('none')

  # Show ticks in the left and lower axes only
  ax.xaxis.set_ticks_position('bottom')
  ax.yaxis.set_ticks_position('left')

  r = int(np.concatenate(vector_names).max())
  plt.xticks(np.arange(-r, r+1, 1))
  plt.yticks(np.arange(-r, r+1, 1))
  
  l = len(ax.get_xticklabels())
  plt.setp(ax.get_xticklabels()[l//2], visible=False)    
  plt.setp(ax.get_yticklabels()[l//2], visible=False)
  
  if not isinstance(vector_names,list): vector_names=[vector_names]
  for i, vector_name in enumerate(vector_names):
    q = ax.quiver(0, 0, vector_name[0], vector_name[1], units='xy' ,scale=1,color=c[i])
  plt.grid()
  ax.set_aspect('equal')

  plt.xlim(-(r+.5),(r+.5))
  plt.ylim(-(r+.5),(r+.5))

# Instructor only
'''
Code di bawah ini hanya untuk visualisasi kita. 
Mahasiswa diharapkan untuk menghitung secara manual operasi 
perkalian matriks dan vektor, 
lalu hasilnya tinggal divisualisasikan.
'''
import math

def rotation(theta):
  rot_matrix = np.array([[math.cos(theta), -math.sin(theta)],
                        [math.sin(theta), math.cos(theta)]])
  return rot_matrix

def draw_polygon(a, b, c, d):
  fig, ax = plt.subplots()
  plt.plot([a[0,0],b[0,0]],[a[1,0],b[1,0]],'k-')
  plt.plot([b[0,0],c[0,0]],[b[1,0],c[1,0]],'k-')
  plt.plot([c[0,0],d[0,0]],[c[1,0],d[1,0]],'k-')
  plt.plot([d[0,0],a[0,0]],[d[1,0],a[1,0]],'k-')
  plt.grid()
  ax.set_aspect('equal')
  plt.xlim(-2,2)
  plt.ylim(-2,2)


## Instructor only

def scaling(scale):
  scale_matrix = np.eye(2)*scale
  return scale_matrix

## Instructor only

def horizontal_shearing(shear):
  shear_matrix = np.array([[1.0, shear], [0.0, 1.0]])
  return shear_matrix

## Instructor only

def vertical_shearing(shear):
  shear_matrix = np.array([[1.0, 0.0], [shear, 1.0]])
  return shear_matrix

def affine_transform(vector_a, theta, translate):
  rotation_matrix = rotation(theta)
  translation_vect = translate

  homogeneous_matrix = np.hstack((rotation_matrix, translation_vect))
  homogeneous_matrix = np.vstack((homogeneous_matrix, np.array([[0.0, 0.0, 1.0]])))

  vector_a = np.vstack((vector_a, np.array([[1.0]])))
  new_vector_a = homogeneous_matrix @ vector_a
  return new_vector_a[0:2,0].reshape((2,1))

def inverse_affine_transform(vector_a, theta, translate):
  rotation_matrix = rotation(theta)
  translation_vect = translate

  homogeneous_matrix = np.hstack((rotation_matrix, translation_vect))
  homogeneous_matrix = np.vstack((homogeneous_matrix, np.array([[0.0, 0.0, 1.0]])))

  vector_a = np.vstack((vector_a, np.array([[1.0]])))
  new_vector_a = np.linalg.inv(homogeneous_matrix) @ vector_a
  return new_vector_a[0:2,0].reshape((2,1))
